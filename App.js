import 'react-native-gesture-handler';
import React from 'react';
import { LogBox } from 'react-native';
import store from './src/reducers/store';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';
import theme from './src/theme';
import MainStack from './stacks/MainStack';

export default function App() {
  LogBox.ignoreLogs(['Warning: ...']);
  return (
    <Provider store={store}>
      <NavigationContainer theme={theme} >
        <MainStack />
      </NavigationContainer>
    </Provider>
  );
}



