const { getDefaultConfig } = require("expo/metro-config");

module.exports = (async () => {
    const {
        resolver: { sourceExts, assetExts }
    } = getDefaultConfig(__dirname);
    return {
        transformer: {
            getTransformOptions: async () => ({
                transform: {
                    experimentalImportSupport: false,
                    inLineRequiries: false
                },
            }),
            babelTransformerPath: require.resolve("react-native-svg-transformer")
        },
        resolver: {
            assetExts: assetExts.filter(ext => ext !== "svg"),
            sourceExts: [...sourceExts, "svg"]
        }
    };
})();



