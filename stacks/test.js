import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import InputMovements from '../src/screen/inputMovements';
import InsertIcon from '../src/assets/insert.svg';

const Tab = createBottomTabNavigator();

export default () => {

    return (
        <Tab.Navigator screenOptions={{
                                  showLabel: false
                                , activeTintColor:   '#D96704'
                                , inactiveTintColor: '#D96704'
                                , style: {
                                    backgroundColor: 'transparent'
                                  , position: 'absolute'
                                  , elevation: 0
                                  , borderTopColor: 'transparent'
                                  , bottom: '5%'
                                  , right: '-65%'
                                }
                        }}
        >
            <Tab.Screen name='InputMovements'
                        component={InputMovements}
                        options={{
                            tabBarIcon: ({ focused, color }) => (
                               <InsertIcon width='50px' height='50px' fill={color} />
                            )
                        }}
            >
            </Tab.Screen>
        </Tab.Navigator>
    )
}