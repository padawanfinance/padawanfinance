import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import PreLoad from '../src/screen/PreLoad';
import SignIn from '../src/screen/SignIn';
import SignUp from '../src/screen/SignUp';
import MainTab from '../src/components/CustomLeftTabBar/LeftTab';

const Stack = createStackNavigator();

export default () => {
    return (
        <Stack.Navigator
        initialRouteName={PreLoad}
        screenOptions={{headerShown: false}}
        >
            <Stack.Screen name='Preload' component={PreLoad} />
            <Stack.Screen name='SignIn'  component={SignIn}  />
            <Stack.Screen name='SignUp'  component={SignUp}  />
            <Stack.Screen name='MainTab'  component={MainTab}  />
        </Stack.Navigator>
    );
}