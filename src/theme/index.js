import React from 'react';
import styled from 'styled-components/native';


//plano de fundo
export const Container = styled.SafeAreaView`
    background-color: #FFA500;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

//logo
export const Image = styled.Image`
    width: 60%;
    height: 40%;
    margin-top: 100px;
`;