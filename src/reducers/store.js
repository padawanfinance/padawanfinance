import {configureStore} from '@reduxjs/toolkit';
import movementReducer from '../reducers/movementSlice';

export default configureStore({
  reducer: {
    movement: movementReducer,
  }
})
