import {createSlice} from '@reduxjs/toolkit';

export const CALENDAR_LOADING = 'CALENDAR_LOADING';
export const CALENDAR_LOADED = 'CALENDAR_LOADED';
export const CALENDAR_DELETED = 'CALENDAR_DELETED';
export const CALENDAR_ERROR = 'CALENDAR_ERROR';

export const slice = createSlice ({
  name: 'movement',
  initialState: {
    movement: {}
  , eventsLoading: false
  , eventsError: false
  , eventsDeleted: false  
  },

  reducers: {
    getMovement(state, {payload}) {
      return {
        ...state
      , movement: payload
      , eventsLoading: false
      , eventsError: false
      , eventsDeleted: false
      }
    },
    deletedMovement(state, {payload}) {
      return {
        ...state
      , movement: payload
      , eventsLoading: false
      , eventsError: false
      , eventsDeleted: true
      }
    },
    loadingMovement(state) {
      return {
        ...state
      , eventsLoading: true
      , eventsError: false
      , eventsDeleted: false
      }
    }  
  }
})

export const {loadingMovement, getMovement, deletedMovement} = slice.actions
export default slice.reducer
export const selectMovement = state => state.movement