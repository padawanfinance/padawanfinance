import { initializeApp, getApp, getApps } from 'firebase/app';

import {
  getAuth
  , onAuthStateChanged
  , signInWithEmailAndPassword
  , createUserWithEmailAndPassword
  , signOut
} from "firebase/auth";

import {
  getFirestore
  , collection
  , collectionGroup
  , addDoc
  , doc
  , setDoc
  , updateDoc
  , arrayUnion
  , deleteDoc
  , arrayRemove
  , query
  , where
  , getDocs
  , getDoc
} from "firebase/firestore";

import { format, add, startOfMonth, lastDayOfMonth } from 'date-fns';

const firebaseConfig = {
  apiKey: "AIzaSyBHPcwnqTkgX8ModUMP3qOXI_x9RUw5Mks",
  authDomain: "financialapp-4cba4.firebaseapp.com",
  databaseURL: "https://financialapp-4cba4-default-rtdb.firebaseio.com",
  projectId: "financialapp-4cba4",
  storageBucket: "financialapp-4cba4.appspot.com",
  messagingSenderId: "796844171390",
  appId: "1:796844171390:web:feeb308b452655ca2ff82f",
  measurementId: "G-5DFWN5L737",
};


const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
const auth = getAuth();

export default {

  checkToken: async (navigation) => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        createCollection(user.uid)
        navigation.navigate("MainTab");
      } else {
        navigation.navigate("SignIn");
      }
    });
  },

  SignIn: async (email, password, navigation) => {
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        navigation.reset({
          routes: [{ name: 'MainTab' }]
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          alert('Senha incorreta!');
        } else {
          alert('Erro: Verifique sua conexão!')
        }
      });
  },

  SignUp: async (email, password, navigation) => {
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        createCollection(userCredential.user.uid);
        navigation.reset({
          routes: [{ name: 'MainTab' }]
        });
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
          alert('Senha fraca.');
        } else {
          alert(errorMessage)
        }
      });
  },

  SignOut: async () => {
    signOut(auth).then(() => {
      // Sign-out successful.
    }).catch((error) => {
      alert(error);
    });
  },

  InsertCategorys: async (category) => {
    let userKey = auth.currentUser.uid;
    try {
      await setDoc(doc(db, 'users', userKey, 'category', category), {});
      console.log("Categoria cadastrada com sucesso!");
    } catch (e) {
      console.error("Erro ao adicionar a Categoria: ", e);
    }
  },

  InsertSubcategory: async (category, subcategory) => {
    let userKey = auth.currentUser.uid;
    try {
      const docRef = doc(db, 'users', userKey, 'category', category);
      await updateDoc(docRef, { subcategory: arrayUnion(subcategory) });
      console.log("Subcategoria cadastrada com sucesso!");
    } catch (e) {
      console.error("Erro ao adicionar a Subcategoria: ", e);
    }
  },

  InsertTag: async (tag) => {
    let userKey = auth.currentUser.uid;
    try {
      await setDoc(doc(db, 'users', userKey, 'tag', tag), {});
      console.log("Tag cadastrada com sucesso!");
    } catch (e) {
      console.error("Erro ao adicionar a Tag: ", e);
    }
  },

  InsertAccount: async (account, type, dueDay) => {
    let userKey = auth.currentUser.uid;
    try {
      const docRef = doc(db, 'users', userKey, 'account', account);
      await setDoc(docRef, {
        type: type
        , dueDay: dueDay
      });
      console.log("Conta cadastrada com sucesso!");
    } catch (e) {
      console.error("Erro ao adicionar a Conta: ", e);
    }
  },

  InsertMovement: async (descryption
    , value
    , dueDay
    , type
    , installments
    , category
    , subcategory
    , account
    , tag) => {
    let userKey = auth.currentUser.uid;

    try {
      var list = [];
      var amount = (parseFloat(value) / installments).toFixed(2);
      var date = null;

      for (let i = 0; i < installments; i++) {
        date = format(add(dueDay, { months: i }), 'dd/MM/yyyy');
        list.push({
          Value: amount,
          DueDate: date,
          paid: "N"
        })
      }

      await addDoc(collection(db, 'users', userKey, 'movement'), {
        descryption: descryption
        , value: value
        , installments: installments
        , category: category
        , subcategory: subcategory
        , account: account
        , tag: tag
        , amount: list
      });
      console.log("Movimento cadastrada com sucesso!");
    } catch (e) {
      console.error("Erro ao adicionar a Movimento: ", e);
    }
  },

  GetCategory: async () => {
    let userKey = auth.currentUser.uid;
    let list = [];
    try {
      const docsSnap = await getDocs(collection(db, 'users/' + userKey + '/category'));
      docsSnap.forEach((doc) => {
        list.push(doc.id);
      });
      return list;
    } catch (e) {
      console.error("Erro ao consultar a categoria: ", e);
    }
  },

  GetSubcategory: async (category) => {
    let userKey = auth.currentUser.uid;
    try {
      const docsSnap = await getDoc(doc(db, 'users/' + userKey + '/category/' + category));
      return docsSnap.data();
    } catch (e) {
      console.error("Erro ao consultar a Subcategoria: ", e);
    }
  },

  GetAccount: async () => {
    let userKey = auth.currentUser.uid;
    let list = [];
    try {
      const docsSnap = await getDocs(collection(db, 'users/' + userKey + '/account'));
      docsSnap.forEach((doc) => {
        list.push(doc.id);
      });
      return list;
    } catch (e) {
      console.error("Erro ao consultar a Conta: ", e);
    }
  },

  GetTag: async () => {
    let userKey = auth.currentUser.uid;
    let list = [];
    try {
      const docsSnap = await getDocs(collection(db, 'users/' + userKey + '/tag'));
      docsSnap.forEach((doc) => {
        list.push(doc.id);
      });
      return list;
    } catch (e) {
      console.error("Erro ao consultar a Tag: ", e);
    }
  },

  GetMovement: async (month) => {
    let userKey = auth.currentUser.uid;
    let list = [];

    const startDate = format(startOfMonth(new Date(month)), 'dd/MM/yyyy')
    const endDate   = format(lastDayOfMonth(new Date(month)), 'dd/MM/yyyy')
    try {
      const docsSnap = await getDocs(collection(db, 'users/' + userKey + '/movement'));

      docsSnap.forEach(async (doc) => {
        doc.data().amount.forEach((amountElement, index) => {
          if (amountElement.DueDate >= startDate && amountElement.DueDate <= endDate) {
            list.push({
              key: doc.id,
              title: doc.data().descryption,
              amount: amountElement.Value,
              date: convertDate(amountElement.DueDate),
              lastInstallment: doc.data().installments,
              account: doc.data().account,
              category: doc.data().category,
              subcategory: doc.data().subcategory,
              tag: doc.data().tag.toString(),
              paid: amountElement.paid,
              icon: "baidu",
              curretInstallment: index + 1,
            })
          }

          function convertDate(inputFormat) {
            function pad(s) { return (s < 10) ? '0' + s : s; }
            let d = new Date(convertFormat(inputFormat));
            return [d.getFullYear(), pad(d.getMonth() + 1), pad(d.getDate())].join('-');
          }

          function convertFormat(inputFormat) {
            let parts = inputFormat.split('/');
            return parts[1] + '/' + parts[0] + '/' + parts[2];
          }

        });

      })

      return list;
    } catch (e) {
      console.error("Erro ao consultar o movimento: ", e);
    }
  },

  DeleteInstallmentMovement: async (date, key, curretInstallment) => {
    let userKey = auth.currentUser.uid;
    let list = [];
    try {
      await deleteDoc(doc(db, 'users/' + userKey + '/movement/', key));
      return list;
    } catch (e) {
      console.error("Não foi possível efetuar a exclusão: ", e);
    }

  }
}

async function createCollection(userId) {
  try {
    await setDoc(doc(db, 'users', userId), {
      name: '',
    });
    console.log("Coleção cadastrada com sucesso!");
  } catch (e) {
    console.error("Erro ao adicionar a Coleção: ", e);
  }
};

