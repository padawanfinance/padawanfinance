import * as React from 'react';
import { createDrawerNavigator} from '@react-navigation/drawer';
import CustomDrawerContent from './LeftTabBar';

import Home from '../../screen/Home';
import Category from '../../screen/Category';
import ExpenseCalendar from '../../screen/ExpenseCalendar';
import Charts from '../../screen/Charts';
import Record from '../../screen/GeneralRecords';
import Profile from '../../screen/Profile';
import Settings from '../../screen/Settings';

const Drawer = createDrawerNavigator();

export default () => {
  return (
    <Drawer.Navigator  screenOptions={{headerShown: false}}
                       drawerContent={(props) => <CustomDrawerContent {...props} />}
     >
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="Category" component={Category} />
      <Drawer.Screen name="ExpenseCalendar" component={ExpenseCalendar} />
      <Drawer.Screen name="Charts" component={Charts}/>
      <Drawer.Screen name="Record" component={Record}/>
      <Drawer.Screen name="Profile" component={Profile} />
      <Drawer.Screen name="Settings" component={Settings} />
    </Drawer.Navigator>
  );
}