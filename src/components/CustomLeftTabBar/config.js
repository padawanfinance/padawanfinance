export const routes = [
    {
      title: 'Principal',
      route: 'Home',
      icon: 'home',
    },

    {
      title: 'Categorias',
      route: 'Category',
      icon: 'pricetag',
    },

    {
      title: 'Calendário de Movimentos',
      route: 'ExpenseCalendar',
      icon: 'calendar',
    },

    {
      title: 'Gráficos',
      route: 'Charts',
      icon: 'bar-chart',
    },

    {
      title: 'Cadastros',
      route: 'Record',
      icon: 'add-circle-sharp',
    },

    {
      title: 'Dados do Usuário',
      route: 'Profile',
      icon: 'person',
    },

    {
      title: 'Configurações',
      route: 'Settings',
      icon: 'options-outline',
    },
    
    {
      title: 'Sair',
      route: 'SignOut',
      icon: 'md-log-out-outline',
    },
  ];
  