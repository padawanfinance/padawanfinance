import React, { useState } from 'react';
import { View, FlatList, TouchableOpacity, Image } from 'react-native';
import { DrawerActions } from '@react-navigation/native';
import { Entypo } from '@expo/vector-icons';
import Firebase from '../../firebase/firebase';
import BabyYoda from '../../assets/baby-yoda.png';

import MenuItem from './MenuItem';
import styles from './styles';
import { routes } from './config';


export default ({ state, navigation }) => {

  const [selected, setSelected] = useState('');

  onPressItem = route => {
    setSelected(() => (
      route
    ));
    if (route === 'SignOut'){
      Firebase.SignOut();
    } else {
      navigation.navigate(route);
    }
  };

  renderMenuItem = ({ item }) => (
    <MenuItem
      id={item.route}
      onPressItem={onPressItem}
      selected={selected === item.route}
      title={item.title}
      icon={item.icon}
    />
  );

  return (
    <View>
      <View style={styles.header.container}>
        <View style={{ justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={() =>
              navigation.dispatch(DrawerActions.toggleDrawer({}))
            }>
            <Entypo name="chevron-thin-left"
              style={styles.header.icon}
            />
          </TouchableOpacity>
        </View>
        <View >
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Home');
            }}>
            <Image source={BabyYoda}
              style={styles.header.avatar} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.content}>
        <FlatList
          initialNumToRender={8}
          data={routes}
          renderItem={renderMenuItem}
          keyExtractor={item => item.route}
        />
      </View>
    </View>
  );
}
