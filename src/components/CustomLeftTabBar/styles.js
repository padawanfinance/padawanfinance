import { Platform } from 'react-native';

export default {

  header: {
    container: {
      marginTop: 40, 
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginLeft: 10,
      marginRight: 15,
      
    },

    avatar: {
      height: 60,
      width: 60,
      borderRadius: Platform.OS === 'android' ? 40 : 20,
    },

    icon: {
      color: '#FFA500',
      fontSize: 26,
    },
  },

  content: {
    paddingTop: Platform.OS === 'android' ? 20 : 30,
    // backgroundColor: 'pink',
  },

  menuItem: {
    
    container: {
      borderColor: 'yellow',
      marginLeft: 0,
    },

    selected: {
      backgroundColor: '#8f1000',
    },

    button: {
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      padding: 15,
      borderBottomWidth: 0.4,
      borderColor: '#FFA500',
    },

      title: {
        // fontFamily: 'Roboto-Light',
          color: '#808080'
      },

    icon: {
      textAlign: 'center',
      width: 30,
      marginRight: 10,
      color: '#FFA500',
      fontSize: Platform.OS === 'android' ? 25 : 30,
    },
    
  },
};
