import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';

const MenuItem = (props) => {

  onPress = () => {
    props.onPressItem(props.id);
  };

  const itemStyle = props.selected
    ? [styles.menuItem.button, styles.menuItem.selected]
    : styles.menuItem.button;

  return (
    <TouchableOpacity style={itemStyle}
      onPress={onPress}>
      <Ionicons name={props.icon}
        style={styles.menuItem.icon} />
      <Text style={props.selected
        ? { color: '#FFA500' }
        : { color: '#808080' }
      }>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
}

MenuItem.propTypes = {
  id: PropTypes.string.isRequired,
  onPressItem: PropTypes.func,
  selected: PropTypes.bool,
  title: PropTypes.string,
  icon: PropTypes.string,
};

export default MenuItem;