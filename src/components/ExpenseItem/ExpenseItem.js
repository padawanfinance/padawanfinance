import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {loadingMovement, deletedMovement, selectMovement} from '../../reducers/movementSlice';
import PropTypes from 'prop-types';
import { Entypo } from '@expo/vector-icons';
import { formatAmount } from '../../utils/formatters';
import styles from './styles';
import { Modalize } from 'react-native-modalize';
import DetailsTransactionModal from '../Modal/ModalDetailsTransaction';
import { Swipeable } from 'react-native-gesture-handler';
import Firebase from '../../firebase/firebase';
const ExpenseItem = (props) => {
  const borderColor = 'red';

  const [modal, setModal] = useState();
  const [toggle, setToggle] = useState(true);
  const modalizeRef = React.createRef();

  const teste = useSelector(selectMovement);
  const dispatch = useDispatch();

  const onOpen = (modalOpen) => {
    setModal(modalOpen);
    modalizeRef.current.open();
  };

  const handleDelete = (date, key, curretInstallment) => {
    dispatch(deletedMovement((
      [teste.movement].map((obj) => ({
        ...obj,
        [date]: obj[date].filter((item) => item.key != key)
      }))))
    )
    Firebase.DeleteInstallmentMovement(date, key, curretInstallment - 1);
  }

  const rightAction = () => {
    return (
      <View style={styles.action.right}>
        <TouchableOpacity style={styles.action.delete}
          onPress={() => handleDelete(props.movement.date, props.movement.key, props.movement.curretInstallment)}
        >
          <Text style={styles.action.text}>Excluir</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.action.edit}>
          <Text style={styles.action.text}>Ediar</Text>
        </TouchableOpacity>
      </View>
    )
  }

  return (
    <Swipeable renderRightActions={rightAction} >
      <TouchableOpacity style={[styles.item.content, { borderColor: borderColor, backgroundColor: '#FFBF00' }, props.style]}
        onPress={() => onOpen('detailsMovement')}
      >
        <View size={7} 
              style={{ flexDirection: 'row' }}
        >
          <Entypo name={props.movement.icon} 
                  size={35} 
                  style={styles.item.icon} />
          <View>
            <Text numberOfLines={2} 
                  style={styles.item.title}
            >
              {props.movement.title}
            </Text>
            <Text numberOfLines={2} 
                  style={styles.item.subtitle}
            >
              {props.movement.category}
            </Text>
          </View>
        </View>
        <View size={3}>
          <Text
            numberOfLines={2}
            style={
              props.movement.typeMovement == "despesa"
                ? styles.item.expenseAmount
                : styles.item.incomeAmount
            }>
            {formatAmount(props.movement.amount)}
          </Text>
        </View>
        <Modalize ref={modalizeRef}
                  adjustToContentHeight={toggle}
                  withReactModal={true}
        >
          {modal == 'detailsMovement' && (<DetailsTransactionModal object={props.movement} modalize={modalizeRef} />)}
          {modal == null && (null)}
        </Modalize>
      </TouchableOpacity>
    </Swipeable>
  );
};

ExpenseItem.propTypes = {
   movement: PropTypes.object.isRequired,
};

export default ExpenseItem;
