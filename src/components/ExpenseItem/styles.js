
export default {

  action: {
    right: {
      justifyContent: 'center',

      marginTop: 20,
      marginRight: 5,
      marginLeft: -10,

      flexDirection: 'row',
    },

    delete: {
      backgroundColor: 'red',
      alignItems: 'center',
      justifyContent: 'center',
      minWidth: 90,
    },

    edit: {
      backgroundColor: 'orange',
      minWidth: 90,
      alignItems: 'center',
      justifyContent: 'center',
    },

    text: {
      color: 'white',
      fontWeight: 'bold',
      fontSize: 15
    }

  },

  item: {
    content: {
      flex: 1,
      elevation: 0,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems:'center',
      paddingTop: 20,
      paddingBottom: 20,
      paddingRight: 15,
      paddingLeft: 15,
      borderLeftWidth: 5,
      borderRightWidth: 0,
      borderTopWidth: 0,
      borderBottomWidth: 0,
      borderRadius: 0,
      backgroundColor: '#FFBF00'
    },
    icon: {
      textAlign: 'center',
      alignItems: 'center',
      color: '#B45F04',
      width: 30,
    },
    title: {
      fontSize: 17,
      color: '#444',
      marginLeft: 15,
    },
    subtitle: {
      fontSize: 16,
      color: '#B45F04',
      marginLeft: 15,
    },
    expenseAmount: {
      fontSize: 20,
      color: '#FF0000',
      alignSelf: 'flex-end',
    },
    incomeAmount: {
      fontSize: 20,
      color: '#228B22',
      alignSelf: 'flex-end',
    },
    swipeBtn: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
  },
};
