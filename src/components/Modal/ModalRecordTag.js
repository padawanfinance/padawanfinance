import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import Firebase from '../../firebase/firebase';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';
import {
    Container
    , ManyColumns
    , ManyLines
    , Label
    , InputText
    , Title
    , Button
    , TitleButton
    , VerticalDivider
} from './styles';


export default function RecordTagModal(props) {
    
    DropDownPicker.setListMode("SCROLLVIEW");
    
    const [value, setValue] = useState(null);

    const modalizeRef = props.modalize;

    const handleInsertTag = () =>{
        Firebase.InsertTag(value);
    }

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }

    };
    return (
        <Container>
            <ManyColumns>
                <Title>CADASTRO DE TAG</Title>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close"
                        size={25}
                        color={'#B7460C'}
                    />
                </TouchableOpacity>
            </ManyColumns>

            <ManyLines>
                <Label>Descrição</Label>
                <InputText onChangeText={txt => setValue(txt)}
                           value={value}
                />
            </ManyLines>

            <ManyColumns>
                <Button activeOpacity={0.75}
                    onPress={() => handleInsertTag()}
                >
                    <TitleButton>CADASTRAR</TitleButton>
                </Button>

                <VerticalDivider />

                <Button activeOpacity={0.75}
                    onPress={handleClose}
                >
                    <TitleButton>CANCELAR</TitleButton>
                </Button>
            </ManyColumns>
        </Container>

    );
}
