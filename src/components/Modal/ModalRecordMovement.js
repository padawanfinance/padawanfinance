import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import Firebase from '../../firebase/firebase';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';
import{ formatCoin } from '../../functions/formatCoin';
import DateField from 'react-native-datefield';
import { format, add } from 'date-fns';
import {
    Container
    , ManyColumns
    , Title
    , ManyLines
    , Label
    , InputText
    , Button
    , TitleButton
    , VerticalDivider
} from './styles';


export default function RecordMovimentModal(props) {
    DropDownPicker.setListMode("SCROLLVIEW");
    DropDownPicker.setMode("BADGE");

    const [movDescryption, setMovDescryption] = useState(null);
    const [movValue, setMovValue] = useState(null);
    const [movDate, setMovDate] = useState(null);

    const [openType, setOpenType] = useState(false);
    const [openInstallment, setOpenInstallment] = useState(false);
    const [openCategory, setOpenCategory] = useState(false);
    const [openSubcategory, setOpenSubcategory] = useState(false);
    const [openAccount, setOpenAccount] = useState(false);
    const [openTag, setOpenTag] = useState(false);

    const [valueType, setValueType] = useState(null);
    const [valueInstallment, setValueInstallment] = useState(null);
    const [valueCategory, setValueCategory] = useState(null);
    const [valueSubcategory, setValueSubcategory] = useState(null);
    const [valueAccount, setValueAccount] = useState(null);
    const [valueTag, setValueTag] = useState([]);

    const [types, setTypes] = useState(
        [
            { label: 'Receita', value: 'receita' },
            { label: 'Despesa', value: 'despesa' }
        ]);

    const [installment, setInstallment] = useState(
        [
            { label: '01X', value: 1 },
            { label: '02X', value: 2 },
            { label: '03X', value: 3 },
            { label: '04X', value: 4 },
            { label: '05X', value: 5 },
            { label: '06X', value: 6 },
            { label: '07X', value: 7 },
            { label: '08X', value: 8 },
            { label: '09X', value: 9 },
            { label: '10X', value: 10 },
            { label: '11X', value: 11 },
            { label: '12X', value: 12 },
        ]);

    const [category, setCategory] = useState([]);
    const [subcategory, setSubcategory] = useState([]);
    const [account, setAccount] = useState([]);
    const [tag, setTag] = useState([]);

    const modalizeRef = props.modalize;

    useEffect(() => {
        const getCategory = () => {
            var list = [];
            Firebase.GetCategory().then(snapshot => {
                if (snapshot) {
                    for(let i = 0; i < snapshot.length; i++){      
                        list.push({ label: snapshot[i], value: snapshot[i] })
                    }
                } 
               setCategory(list)
            })
        };
    getCategory()}, [])

    const handleGetSubcategory = () => {
            var list = [];
            Firebase.GetSubcategory(valueCategory).then(snapshot => {
                if (snapshot) {
                    for(let i = 0; i < snapshot['subcategory'].length; i++){ 
                        list.push({ label: snapshot['subcategory'][i], value: snapshot['subcategory'][i] })
                    } 
                }
                setSubcategory(list)
            })   
        };


    useEffect(() => {
        const getTag = () => {
            var list = [];
            Firebase.GetTag().then(snapshot => {
                for(let i = 0; i < snapshot.length; i++){      
                    list.push({ label: snapshot[i], value: snapshot[i] })
                }
               setTag(list)
            })
        };
    getTag() }, [])

    useEffect(() => {
      const getAccount = () => {
          var list = [];
          Firebase.GetAccount().then(snapshot => {
            for(let i = 0; i < snapshot.length; i++){      
                list.push({ label: snapshot[i], value: snapshot[i] })
            }
           setAccount(list)
        })
    };
    getAccount()}, [])

    const handleInsertMovement = () => {
        var tempCategory = null;
        var installment = [];

        category.filter((items) => {return items.value == valueCategory})
                .map((obj) => {return tempCategory = obj.label})

        if (movDescryption == null){
            alert('Informe a descrição!')
        }else if (movValue == null){
            alert('Informe o valor!')
        }else if (movDate == null){
            alert('Informe a data!')
        }else if (valueType == null){
            alert('Informe o tipo!')
        }else if (valueInstallment == null){
            alert('Informe a parcela!')
        }else if (tempCategory == null){
            alert('Informe a categoria!')
        }else if (valueAccount == null){
            alert('Informe a conta!')
        }else {

        Firebase.InsertMovement(movDescryption
                              , movValue
                              , movDate
                              , valueType
                              , valueInstallment
                              , tempCategory
                              , valueSubcategory
                              , valueAccount
                              , valueTag
                              , installment
                              )
    };
}

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };
    
    return (
        <Container>
            <ManyColumns>
                <Title>MOVIMENTOS FINANCEIROS</Title>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close" 
                              size={25} 
                              color={'#B7460C'} />
                </TouchableOpacity>
            </ManyColumns>

            <ManyLines>
                <Label>Descrição</Label>
                <InputText onChangeText={txt => setMovDescryption(txt)}
                           value={movDescryption}
                />
            </ManyLines>

            <ManyColumns>
                <ManyLines>
                    <Label>Valor</Label>
                    <InputText onChangeText={txt => setMovValue(formatCoin(txt))}
                           value={movValue}
                           keyboardType={'numeric'}
                           maxLength={10}
                    />
                </ManyLines>
                <ManyLines>
                    <Label>Data</Label>
                    <DateField
                               disabled
                               onSubmit={txt => {setMovDate(txt)}}
                               defaultValue={movDate}
                               labelDate=""
                               labelMonth=""
                               labelYear=""
                               styleInput={{  fontSize: 18, color: '#A0A0A0' }}
                               containerStyle={{ borderBottomWidth: 1
                                               , borderBottomColor: '#A0A0A0'
                                               , justifyContent: 'flex-start'
                                               , 
                                                }}
                     />
                    </ManyLines>
            </ManyColumns>

            <ManyColumns >
                <ManyLines>
                    <Label>Tipo</Label>
                    <DropDownPicker
                        style={{ height: 30, width: '100%', borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                        open={openType}
                        value={valueType}
                        items={types}
                        setOpen={setOpenType}
                        setValue={setValueType}
                        setItems={setTypes}
                        textStyle={{ fontSize: 18, color: '#808080' }}
                        placeholder="---"
                        searchable={true}
                        searchPlaceholder="Procurar"
                        dropDownDirection="TOP"
                    />
                </ManyLines>
                <ManyLines>
                    <Label>Parcela</Label>
                    <DropDownPicker
                        style={{ height: 30, width: '100%', borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                        open={openInstallment}
                        value={valueInstallment}
                        items={installment}
                        setOpen={setOpenInstallment}
                        setValue={setValueInstallment}
                        setItems={setInstallment}
                        textStyle={{ fontSize: 18, color: '#808080' }}
                        placeholder="---"
                        searchable={true}
                        searchPlaceholder="Procurar"
                        dropDownDirection="TOP"
                        closeAfterSelecting={true}
                        
                    />
                </ManyLines>
            </ManyColumns>

            <ManyColumns >
                <ManyLines>
                    <Label>Categoria</Label>
                    <DropDownPicker
                        style={{ height: 40, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                        open={openCategory}
                        value={valueCategory}
                        items={category}
                        setOpen={setOpenCategory}
                        setValue={setValueCategory}
                        setItems={setCategory}
                        textStyle={{ fontSize: 18, color: '#808080' }}
                        placeholder="Selecionar"
                        searchable={true}
                        searchPlaceholder="Procurar"
                        dropDownDirection="TOP"
                        maxHeight={140}
                    />
                </ManyLines>
                <ManyLines>
                    <Label>Subcategoria</Label>
                    <DropDownPicker
                        style={{ height: 40, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                        open={openSubcategory}
                        value={valueSubcategory}
                        items={subcategory}
                        setOpen={setOpenSubcategory}
                        setValue={setValueSubcategory}
                        setItems={setSubcategory}
                        onPress={() => {handleGetSubcategory()}}
                        textStyle={{ fontSize: 18, color: '#808080' }}
                        placeholder="Selecionar"
                        searchable={true}
                        searchPlaceholder="Procurar"
                        dropDownDirection="TOP"
                        maxHeight={140}
                        //disabled={true}
                    />
                </ManyLines>
            </ManyColumns >
                
            <ManyLines>
                <Label>Conta</Label>
                <DropDownPicker
                    style={{ height: 30, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                    open={openAccount}
                    value={valueAccount}
                    items={account}
                    setOpen={setOpenAccount}
                    setValue={setValueAccount}
                    setItems={setAccount}
                    textStyle={{ fontSize: 18, color: '#808080' }}
                    placeholder="---"
                    searchable={true}
                    searchPlaceholder="Procurar"
                    dropDownDirection="TOP"
                />
            </ManyLines>

            <ManyLines>
                <Label>Tag</Label>
                <DropDownPicker
                    style={{ height: 40, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                    multiple={true}
                    open={openTag}
                    value={valueTag}
                    items={tag}
                    setOpen={setOpenTag}
                    setValue={setValueTag}
                    setItems={setTag}
                    textStyle={{ fontSize: 18, color: '#808080' }}
                    placeholder="---"
                    searchable={true}
                    searchPlaceholder="Procurar"
                    dropDownDirection="TOP"
                    categorySelectable={true}
                />
            </ManyLines>

            <ManyColumns>
                <Button activeOpacity={0.75} 
                        onPress={() => handleInsertMovement()}
                >
                    <TitleButton>CADASTRAR</TitleButton>
                </Button>

                <VerticalDivider />

                <Button activeOpacity={0.75} 
                        onPress={handleClose}
                >
                    <TitleButton>CANCELAR</TitleButton>
                </Button>
            </ManyColumns>
        </Container>

    );
}
