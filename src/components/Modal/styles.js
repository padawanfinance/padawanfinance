import React from 'react';
import styled from 'styled-components';

export const Container = styled.View`
    padding: 20px;
`;

export const ManyColumns = styled.View`
    flex-direction: row;
    justify-content: space-between; 
    margin-bottom: 10px;
`;

export const ManyLines = styled.View`
    margin-bottom: 10px;
    flex: 1px;
`;

export const MiddleLeft = styled.View`
    flex-direction: row;
    justify-content: flex-start; 
    flex: 0.55;
`;

export const MiddleRight = styled.View`
    flex-direction: row;
    justify-content: flex-start; 
    flex: 0.45;
`;

export const Title = styled.Text`
    margin-bottom: 10px;
    font-size: 16px;
    font-weight: 600;
    color: #B7460C;
`;

export const Label = styled.Text`
    font-size: 18px;
    font-weight: 600;
    color: #808080;
    padding-left: 5px;
    font-weight: bold;
    margin-bottom: 5px;
`;

export const InputText = styled.TextInput`
    width: 100%;
    border-bottom-color: #A9A9A9;
    border-radius: 10px;
    border-bottom-width: 1px;
    padding-left: 5px;
    font-size: 18px;
    color: #808080;
`

export const Button = styled.TouchableOpacity`
    padding: 10px;
    width: 50%;
    margin-top: 10px;
`;

export const TitleButton = styled.Text`
    color: #B7460C;
    font-size: 15px;
    font-weight: 600;
    text-align: center;
`
export const VerticalDivider = styled.View`
      background-color: #A9A9A9;
      width: 1.5px;
      margin-top: 10px;
`;

export const AreaIcon = styled.View`
    min-width: 30px;
`;

export const AreaText = styled.View`

`;

  export const LabelDescryption = styled.Text`
    font-size: 18px;
    font-weight: 200;
    line-height: 22px;
    color: #808080;
    
`;

export const ManyColumnsPlots = styled.View`
    flex-direction: row;
    justify-content: space-between; 
    margin-bottom: 3px;
`;

export const LabelPlots = styled.Text`
    font-size: 14px;
    font-weight: 600;
    color: #808080;
    padding-left: 5px;
    font-weight: bold;
    margin-bottom: 5px;
`;