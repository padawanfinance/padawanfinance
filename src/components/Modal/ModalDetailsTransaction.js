import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import { format } from 'date-fns';

import {
      Container
    , Title
    , ManyColumns
    , ManyLines
    , MiddleLeft
    , MiddleRight
    , Button
    , AreaIcon
    , AreaText
    , Label
    , LabelDescryption
    , TitleButton
    , VerticalDivider
    , LabelPlots
    , ManyColumnsPlots
} from './styles';


export default function DetailsTransactionModal(props) {
    const modalizeRef = props.modalize;

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };

    return (
        <Container>

            <ManyColumns>
                <Title>DETALHES DO MOVIMENTO</Title>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close"
                        size={25}
                        color={'#A9A9A9'}
                    />
                </TouchableOpacity>
            </ManyColumns>


            <ManyLines>
                <MiddleLeft>
                    <AreaIcon>
                        <Ionicons name="wallet"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Descrição</Label>
                        <LabelDescryption>{props.object.title}</LabelDescryption>
                    </AreaText>
                </MiddleLeft>
            </ManyLines>


            <ManyLines>
                <MiddleLeft>
                    <AreaIcon>
                        <Ionicons name="bookmark" 
                                  size={25} 
                                  color={'#A9A9A9'} 
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Conta</Label>
                        <LabelDescryption> { props.object.account } </LabelDescryption>
                    </AreaText>
                </MiddleLeft>
            </ManyLines>


            <ManyColumns>

                <MiddleLeft>
                    <AreaIcon>
                        <Ionicons name="cash" 
                                  size={25} 
                                  color={'#A9A9A9'} 
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Valor</Label>
                        <LabelDescryption>{props.object.amount}</LabelDescryption>
                    </AreaText>
                </MiddleLeft>

                <MiddleRight>
                    <AreaIcon>
                        <Ionicons name="calendar"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Data</Label>
                        <LabelDescryption>{format(new Date(props.object.date), 'dd/MM/yyyy')}</LabelDescryption>
                    </AreaText>
                </MiddleRight>

            </ManyColumns>

            <ManyColumns>

                <MiddleLeft>
                    <AreaIcon>
                        <Ionicons name="wallet"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Parcela</Label>
                        <LabelDescryption>{props.object.curretInstallment + " de " + props.object.lastInstallment}</LabelDescryption>
                    </AreaText>
                </MiddleLeft>

                <MiddleRight>
                    <AreaIcon>
                        <Ionicons name="document-text"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Tipo</Label>
                        <LabelDescryption>{props.object.typeMovement}</LabelDescryption>
                    </AreaText>
                </MiddleRight>

            </ManyColumns>

            <ManyColumns>

                <MiddleLeft>
                    <AreaIcon>
                        <Ionicons name="bookmark-sharp"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Categoria</Label>
                        <LabelDescryption>{props.object.category}</LabelDescryption>
                    </AreaText>
                </MiddleLeft>

                <MiddleRight>
                    <AreaIcon>
                        <Ionicons name="bookmarks"
                            size={25}
                            color={'#A9A9A9'}
                        />
                    </AreaIcon>
                    <AreaText>
                        <Label>Subcategoria</Label>
                        <LabelDescryption>{props.object.subcategory}</LabelDescryption>
                    </AreaText>
                </MiddleRight>

            </ManyColumns>

            {/* <ManyColumnsPlots>
                <Title>DETALHES DAS PROXIMAS 3 PARCELAS</Title>
            </ManyColumnsPlots>
            <ManyColumnsPlots>
                <LabelPlots>Parcela 1</LabelPlots>
                <LabelDescryption>01/02/2021</LabelDescryption>  
                <LabelDescryption>R$ 100,00</LabelDescryption>
            </ManyColumnsPlots>
            <ManyColumnsPlots>
                <LabelPlots>Parcela 3</LabelPlots>
                <LabelDescryption>01/03/2021</LabelDescryption>  
                <LabelDescryption>R$ 100,00</LabelDescryption>
            </ManyColumnsPlots>
            <ManyColumnsPlots>
                <LabelPlots>Parcela 3</LabelPlots>
                <LabelDescryption>01/04/2021</LabelDescryption>  
                <LabelDescryption>R$ 100,00</LabelDescryption>
            </ManyColumnsPlots> */}

            {/* <ManyColumns>
                <Button  activeOpacity={0.75} 
                         onPress={() => {handleEditMovement()}}
                >
                    <TitleButton >EDITAR</TitleButton>
                </Button>

                <VerticalDivider />

                <Button activeOpacity={0.75} 
                        onPress={handleClose}
                >
                    <TitleButton>EXCLUIR</TitleButton>
                </Button>
            </ManyColumns> */}
        </Container>


    );
}

