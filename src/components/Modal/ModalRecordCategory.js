import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import Firebase from '../../firebase/firebase';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';

import {Container
      , ManyColumns
      , ManyLines
      , Title
      , Label
      , InputText
      , Button
      , TitleButton 
      , VerticalDivider
    } from './styles'


export default function RecordCategoryModal(props) {
  
  // DropDownPicker.setListMode("SCROLLVIEW");
  
  const [value, setValue] = useState();
  const [open, setOpen] = useState(false);

  // const [valueType, setValueType] = useState();
  // const [items, setItems] = useState(
  //   [
  //      { label: 'Receita', value: 'receita' },
  //   {   label: 'Despesa', value: 'despesa' }
  //   ]);
  
  const modalizeRef = props.modalize;

  const handleClose = () => {
    if (modalizeRef.current) {
      modalizeRef.current.close();
    }
  };

  const handleInsertCategory = () => {
    Firebase.InsertCategory(value);
  }
  
  return (
    <Container >

      <ManyColumns >
        <Title>CADASTRO DE CATEGORIA</Title>
        <TouchableOpacity onPress={handleClose}>
          <Ionicons name="close" 
                    size={25} 
                    color={'#B7460C'} 
          />
        </TouchableOpacity>
      </ManyColumns>

      <ManyLines> 
        <Label>Descrição</Label>
        <InputText onChangeText={txt => setValue(txt)}
                    value={value}/>
      </ManyLines>

      {/* <ManyLines>
        <Label>Tipo</Label>
        <DropDownPicker
          style={{ height: 30, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
          open={open}
          value={value}
          items={items}
          setOpen={setOpen}
          setValue={setValue}
          setItems={setItems}
          textStyle={{ fontSize: 18, color: '#808080' }}
          placeholder="Selecione um Tipo"
          dropDownDirection="TOP"
          disableBorderRadius={true}
          //searchable={true}
          //searchPlaceholder="Procurar"
        />
      </ManyLines> */}

      <ManyColumns>
        <Button activeOpacity={0.75} 
                onPress={() => handleInsertCategory()}
        >
          <TitleButton>CADASTRAR</TitleButton>
        </Button>

        <VerticalDivider/>

        <Button activeOpacity={0.75} 
                onPress={handleClose}
        >
          <TitleButton>CANCELAR</TitleButton>
        </Button>
      </ManyColumns>
    </Container>

  );
}

