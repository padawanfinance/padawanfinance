import React, { useState } from 'react';
import { TouchableOpacity } from 'react-native';
import Firebase from '../../firebase/firebase';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';

import {
    Container
    , ManyColumns
    , Title
    , ManyLines
    , Label
    , InputText
    , Button
    , TitleButton
    , VerticalDivider
} from './styles';


export default function RecordAccountModal(props) {
    
    DropDownPicker.setListMode("SCROLLVIEW");
    
    const [account, setAccount] = useState(null);
    const [dueDay, setDueDay] = useState(null);
    const [open, setOpen] = useState(false);
    const [keyAccount, setKeyAccount] = useState(null);
    const [items, setItems] = useState(
             [
                 {label: 'Débito', value:'Débito'},
                 {label: 'Crédito', value:'Crédito'}
             ]
    );

    const modalizeRef = props.modalize;

    const handleSetAccount = () => {
        if (dueDay > 31){
            alert('Selecione dias entre 1 e 31!')
        }else if (keyAccount == null) {
            alert('Selecione um tipo!')
        }else {
            Firebase.InsertAccount(account, keyAccount, dueDay);
        }
    }

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };

    return (
        <Container>
            <ManyColumns>
                <Title>CADASTRO DE CONTA</Title>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close"
                              size={25}
                              color={'#B7460C'}
                    />
                </TouchableOpacity>
            </ManyColumns>

            <ManyLines>
                <Label>Descrição</Label>
                <InputText onChangeText={txt => setAccount(txt)}
                           value={account}
                />
            </ManyLines>

            <ManyLines>
                <Label>Tipo</Label>
                <DropDownPicker
                    style={{ height: 40, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                    open={open}
                    value={keyAccount}
                    items={items}
                    setOpen={setOpen}
                    setValue={setKeyAccount}
                    setItems={setItems}
                    textStyle={{ fontSize: 18, color: '#808080' }}
                    placeholder="Selecione um Tipo"
                    searchable={true}
                    searchPlaceholder="Procurar"
                    dropDownDirection="TOP"
                    maxHeight={140}
                />
            </ManyLines>
        
            <ManyLines>
                <Label>Dia do Vencimento</Label>
                <InputText onChangeText={txt => setDueDay(txt.replace(/\D/g, ''))}
                           value={(keyAccount != 'Crédito'? "": dueDay )}
                           keyboardType='decimal-pad'
                           maxLength={2}
                           editable={(keyAccount != 'Crédito'? false: true )}
                />
            </ManyLines>

            <ManyColumns>
                <Button activeOpacity={0.75}
                        onPress={() => handleSetAccount()}
                >
                    <TitleButton>CADASTRAR</TitleButton>
                </Button>

                <VerticalDivider />

                <Button activeOpacity={0.75}
                        onPress={handleClose}
                >
                    <TitleButton>CANCELAR</TitleButton>
                </Button>
            </ManyColumns>
        </Container>

    );
}
