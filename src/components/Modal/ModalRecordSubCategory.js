import React, { useState, useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import Firebase from '../../firebase/firebase';
import { Ionicons } from "@expo/vector-icons";
import DropDownPicker from 'react-native-dropdown-picker';

import {
    Container
    , ManyLines
    , Title
    , ManyColumns
    , Label
    , InputText
    , Button
    , TitleButton
    , VerticalDivider
} from './styles';


export default function RecordSubCategoryModal(props) {
    
    DropDownPicker.setListMode("SCROLLVIEW");
    
    const [open, setOpen] = useState(false);
    const [items, setItems] = useState([]);
    const [keyCategory, setKeyCategory] = useState(null);
    const [subcategory, setSubcategory] = useState(null)
    
    const modalizeRef = props.modalize;

    const handleClose = () => {
        if (modalizeRef.current) {
            modalizeRef.current.close();
        }
    };

    const handleInsertSubcategoria = () => {
        if (items.length != 0 && keyCategory != null) {
            Firebase.InsertSubcategory(keyCategory, subcategory);
        }else {
            alert("Selecione uma categoria!")
        }
        
    };

        useEffect(() => {
            const getCategory = () => {
                var list = [];
                Firebase.GetCategory().then(snapshot => {
                    console.log(snapshot)
                    for(let i = 0; i < snapshot.length; i++){    
                        list.push({ label: snapshot[i], value: snapshot[i] })
                    } 
                    setItems(list);   
                })
            };
        getCategory()
      }, [])


    return (
        <Container>
            <ManyColumns>
                <Title>CADASTRO DE SUBCATEGORIA</Title>
                <TouchableOpacity onPress={handleClose}>
                    <Ionicons name="close"
                        size={25}
                        color={'#B7460C'}
                    />
                </TouchableOpacity>
            </ManyColumns>

            <ManyLines>
                <Label>Descrição</Label>
                <InputText onChangeText={txt => setSubcategory(txt)}
                           value={subcategory}/>
            </ManyLines>

            <ManyLines>
                <Label>Categoria Raiz</Label>
                <DropDownPicker
                    style={{ height: 40, borderColor: '#A9A9A9', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0 }}
                    open={open}
                    value={keyCategory}
                    items={items}
                    setOpen={setOpen}
                    setValue={setKeyCategory}
                    setItems={setItems}
                    textStyle={{ fontSize: 18, color: '#808080' }}
                    placeholder="Selecione uma Categoria Raiz"
                    searchable={true}
                    searchPlaceholder="Procurar"
                    dropDownDirection="TOP"
                    maxHeight={140}
                />
            </ManyLines>

            <ManyColumns>
                <Button activeOpacity={0.75}
                    onPress={() => handleInsertSubcategoria()}
                >
                    <TitleButton>CADASTRAR</TitleButton>
                </Button>

                <VerticalDivider />

                <Button activeOpacity={0.75}
                    onPress={handleClose}
                >
                    <TitleButton>CANCELAR</TitleButton>
                </Button>
            </ManyColumns>
        </Container>

    );
}

