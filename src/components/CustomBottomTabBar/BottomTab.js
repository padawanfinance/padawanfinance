import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../../screen/Home';
import InputMovements from '../../screen/inputMovements';
import ListMovement from '../../screen/listMovement';
import BottomTabBar from '.././CustomBottomTabBar/BottomTabBar';

const Tab = createBottomTabNavigator();

export default () => {
    return (
        <Tab.Navigator initialRouteName='Home' 
                       screenOptions={{headerShown: false}}
                       tabBar={props => <BottomTabBar {...props}/>}>
            <Tab.Screen name='InputMovements' component={InputMovements} />
            <Tab.Screen name='Home' component={Home} />
            <Tab.Screen name='ListMovement' component={ListMovement} />
        </Tab.Navigator>
    )
}