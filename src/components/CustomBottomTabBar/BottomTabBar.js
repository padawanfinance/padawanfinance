import React from 'react';
import styled from 'styled-components/native';
import InsertIcon from '../../assets/insert.svg';
import HomeIcon from '../../assets/home.svg';
import MenuIcon from '../../assets/list.svg';

const TabArea = styled.View`
    height: 50px;
    flex-direction: row;
    background-color:  #FFBF00;
`;
  
const TabItem = styled.TouchableOpacity`
    flex: 1;
    align-items: center;
    justify-content: center;
`;

const TabItemCenter = styled.TouchableOpacity`
    width: 60px;
    height: 60px;
    justify-content: center;
    align-items: center;
    background-color: #FFA500;
    border-radius: 35px;
    border: 3px solid #FFBF00;
    margin-top: -20px;
`;

export default ({state, navigation}) => {

    const goTo = (screenName) => {
        navigation.navigate(screenName);
    }

    return (
        <TabArea>
            <TabItem onPress={() => goTo('InputMovements')}>
                <InsertIcon width="35" height="35" style={{opacity: state.index === 0? 1: 0.5}}/>
            </TabItem>
            <TabItemCenter onPress={() => goTo('Home')}>
                <HomeIcon width="30" height="30" style={{opacity: state.index === 1? 1: 0.5}} />
            </TabItemCenter>
            <TabItem onPress={() => goTo('ListMovement')}>
                <MenuIcon width="30" height="30" style={{opacity: state.index === 2? 1: 0.5}}/>
            </TabItem>
        </TabArea>
    )
}