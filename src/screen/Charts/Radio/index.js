import React from 'react';
import RadioForm from 'react-native-simple-radio-button';

import {styles,
    RADIO_BUTTON_COLOR,
    RADIO_LABEL_COLOR,
    SELECTED_BUTTON_COLOR,
    SELECTED_LABEL_COLOR
} from '../styles';

export default (props) => {
    return (
        <RadioForm
            radio_props={props.radio}
            formHorizontal={true}
            labelHorizontal={false}
            initial={0}
            buttonColor={RADIO_BUTTON_COLOR}
            labelColor={RADIO_LABEL_COLOR}
            selectedButtonColor={SELECTED_BUTTON_COLOR}
            selectedLabelColor={SELECTED_LABEL_COLOR}
            onPress={(value) => props.selectedChart(value)}
            style={styles.radio_position}
        />
    )
}