import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';

import {
    styles,
    ITEM_BACKGROUND_COLOR,
    ITEM_COLOR,
    ITEM_SELECTED_COLOR
} from "../styles";

export default (props) => {
    let data = props.categories;

    const renderItem = ({ item }) => (
        <TouchableOpacity
            style={{
                ...styles.list_item_selected, 
                ...styles.shadow,
                backgroundColor:
                    (props.selectedCategory && props.selectedCategory.name == item.name)
                        ? item.color
                        : ITEM_BACKGROUND_COLOR
            }}

            onPress={() => {
                let category = data.filter(a => a.name == item.name);
                props.setCategory(category[0]);
            }}
        >
            {/* Name/Category */}
            <View style={{
                ...styles.continer_category,
                borderColor:
                    (props.selectedCategory && props.selectedCategory.name == item.name)
                        ? ITEM_SELECTED_COLOR
                        : item.color
            }}>

                <Text style={{
                    marginLeft: 8,
                    color:
                        (props.selectedCategory && props.selectedCategory.name == item.name)
                            ? ITEM_SELECTED_COLOR
                            : ITEM_COLOR
                }}
                >{item.name}
                </Text>
            </View>

            {/* Expenses */}
            <View style={{ justifyContent: 'center' }}>
                <Text style={{
                    color:
                        (props.selectedCategory && props.selectedCategory.name == item.name)
                            ? ITEM_SELECTED_COLOR
                            : ITEM_COLOR
                }}
                >
                    R$ {item.y} - {item.label}
                </Text>
            </View>
        </TouchableOpacity>
    )

    return (
        <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={item => `${item.id}`}
        />
    )
}