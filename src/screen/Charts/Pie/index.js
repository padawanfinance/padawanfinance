import React from 'react';
import { Svg } from 'react-native-svg';
import { VictoryPie, VictoryLabel } from 'victory-native';

import {
    styles,
    width,
    SELECTED_SLICE,
    RADIUS,
    INNER_RADIUS,
    LABEL_RADIUS,
    LABEL_COLOR
} from '../styles';

export default (props) => {

    let categories = props.categories;
    let colorScales = categories.map((item) => item.color);
    let totalExpenseCount = categories.reduce((a, b) => a + (b.expenseCount || 0), 0);

    return (
        <Svg width={width} height={width-4}>
            <VictoryPie
                standalone={false} // Android workaround
                data={categories}
                colorScale={colorScales}

                labels={(datum) => `${datum.y}`}

                radius={({ datum }) =>
                    (props.selectedCategory && props.selectedCategory.name == datum.name)
                        ? SELECTED_SLICE
                        : RADIUS
                }

                innerRadius={INNER_RADIUS}

                labelRadius={LABEL_RADIUS}

                events={[{
                    target: "data",
                    eventHandlers: {
                        onPress: () => {
                            return [{
                                target: "labels",
                                mutation: (prop) => {
                                    let categoryName = categories[prop.index].name;
                                    let category = categories.filter(a => a.name == categoryName);
                                    props.setCategory(category[0]);
                                }
                            }]
                        }
                    }
                }]}
            />
            <VictoryLabel
                textAnchor="middle" verticalAnchor="middle"
                x={width / 2} y={width / 2}
                style={[{ fontSize: 20, fill: '#A0522D' }
                      , { fontSize: 15, fill: '#A0522D' }
                ]}
                text={[totalExpenseCount, 'Despesas']}
            />
        </Svg>
    )
}

