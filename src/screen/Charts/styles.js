import React from 'react';
import { StyleSheet, Dimensions } from 'react-native';

export const width = Dimensions.get("window").width;

//NOTA: Declarar as constantes dentro do Chart com condicional caso seja null

//NOTA 2: Declarar o Style dentro do Chart.

//chart pie
export const RADIUS = (width) * 0.4 - 25;
export const SELECTED_SLICE = (width) * 0.4 - 15;
export const LABEL_COLOR = '#FFFFFF';
export const INNER_RADIUS = 70;
export const LABEL_RADIUS = (width * 0.4 + INNER_RADIUS) / 2.5;

//chart bar
export const BAR_CHART_W = (width);
export const BAR_CHART_H = (width) * 0.84;
export const BAR_PADDING = (width) * 0.03;
export const BAR_WIDTH = (width) * 0.07;
export const SELECTED_BAR = '#000';
export const SELECTED_BORDER = (width) * 0.005;

//radio chart
export const RADIO_BUTTON_COLOR = '#FFC500';
export const RADIO_LABEL_COLOR = '#771513';
export const SELECTED_BUTTON_COLOR = '#771513';
export const SELECTED_LABEL_COLOR = '#771513';

//List expense
export const ITEM_BACKGROUND_COLOR = '#FFB500';
export const ITEM_COLOR = '#771513';
export const ITEM_SELECTED_COLOR = '#FFFFFF';

export const styles = StyleSheet.create({

    container_bar: {
        marginTop: width * 0.1,
        maxWidth: width * 0.94,
        marginBottom: width * 0.05,
        alignSelf: 'center',
        backgroundColor: '#FFB500'
    },

    radio_position: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    continer_category: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        borderLeftWidth: 5,
        borderRadius: 10,
    },

    list_item_selected: {
        flexDirection: 'row',
        height: 50,
        borderRadius: 10,
        marginHorizontal: 5,
        marginBottom: 10,
        paddingRight: 5
    },



    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.10,
        shadowRadius: 1.41,

        elevation: 1,
    }
})

