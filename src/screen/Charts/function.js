import React from 'react';

export function processCategoryDataToDisplay(categories) {
    // Filtra as despesas com o status "confirmado"
        let chartData = categories.map((item) => {
        let confirmExpenses = item.expenses.filter(a => a.status == "C")
        var total = confirmExpenses.reduce((a, b) => a + (b.total || 0), 0)

        return {
            id: item.id,
            name: item.name,
            y: total,
            expenseCount: confirmExpenses.length,
            color: item.color,
        }
    })

    // filtrar categorias sem dados / despesas
    let filterChartData = chartData.filter(a => a.y > 0)

    // Calcula o total das despesas
    let totalExpense = filterChartData.reduce((a, b) => a + (b.y || 0), 0)

    // Calcular a porcentagem e preencher novamente os dados do gráfico
    let finalChartData = filterChartData.map((item) => {
        let percentage = (item.y / totalExpense * 100).toFixed(0)
        return {
            name: item.name,
            id: item.id,
            label: `${percentage}%`,
            y: Number(item.y),
            expenseCount: item.expenseCount,
            color: item.color,
        }
    })
    return finalChartData
}