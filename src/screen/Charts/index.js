import React, { useState } from "react";

import List from './List';
import Pie from './Pie';
import Bar from './Bar';
import Radio from './Radio';

import { categoriesData } from './mock';
import { processCategoryDataToDisplay } from './function';
import { ScrollView, View } from 'react-native';


export default () => {

    const [categories, setCategories] = useState(categoriesData);
    const chartData = processCategoryDataToDisplay(categories);
    const [selectedCategory, setSelectedCategory] = useState(null);


    let [chart, setChart] = useState(0);
    let radio = [
        { label: 'Pizza', value: 0 },
        { label: 'Barra', value: 1 }
    ]

    return (
        <View style={{ flex: 1, backgroundColor: '#FFA500' }}>
                {chart === 0
                    ? <Pie categories={chartData}
                        setCategory={setSelectedCategory}
                        selectedCategory={selectedCategory}
                    />
                    : null
                }
                {chart === 1
                    ? <Bar categories={chartData}
                        setCategory={setSelectedCategory}
                        selectedCategory={selectedCategory}
                    />
                    : null}

                <Radio radio={radio} selectedChart={setChart} />
                <List categories={chartData}
                    setCategory={setSelectedCategory}
                    selectedCategory={selectedCategory}
                />
        </View>

    )
}



