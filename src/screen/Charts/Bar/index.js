import React from 'react';
import { Svg } from 'react-native-svg';
import { VictoryChart, VictoryBar , VictoryAxis} from 'victory-native';

import {
    styles,
    width,
    BAR_CHART_H,
    BAR_CHART_W,
    BAR_PADDING,
    BAR_WIDTH,
    LABEL_COLOR,
    SELECTED_BAR,
    SELECTED_BORDER
} from '../styles';

export default (props) => {

    let categories = props.categories;

    return (
        <Svg width={BAR_CHART_W} style={{...styles.container_bar, 
                                         ...styles.shadow}}
        >
            <VictoryBar
                standalone={false} // Android workaround
                data={categories}
                domainPadding={{ x: BAR_PADDING }}
                barWidth={BAR_WIDTH}
                height={BAR_CHART_H}
                width={BAR_CHART_W}
                labels={(datum) => `${datum.y}`}

                x={(datum) => `${datum.name}`}

                style={{
                    labels: { fill: LABEL_COLOR },

                    data: {
                        fill: ({ datum }) => datum.color
                        ,
                        stroke: ({ datum }) =>
                            (props.selectedCategory && props.selectedCategory.name == datum.name)
                                ? SELECTED_BAR
                                : null
                        ,
                        strokeWidth: ({ datum }) =>
                            (props.selectedCategory && props.selectedCategory.name == datum.name)
                                ? SELECTED_BORDER
                                : null
                    }
                }}

                events={[{
                    target: "data",
                    eventHandlers: {
                        onPress: () => {
                            return [{
                                target: "data",
                                mutation: (prop) => {
                                    let categoryName = categories[prop.index].name;
                                    let category = categories.filter(a => a.name == categoryName);
                                    props.setCategory(category[0]);
                                }
                            }]
                        }
                    }
                }]}
            />
            <VictoryAxis height={BAR_CHART_H} offsetX={45} dependentAxis />
        </Svg>
    )
}