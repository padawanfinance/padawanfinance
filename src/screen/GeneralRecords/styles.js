import React from 'react';
import styled from 'styled-components';

export const Container = styled.SafeAreaView`
    background-color: #FFA500;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const Button = styled.TouchableOpacity`
    width: 40%;
    height: 150px;
    align-Items: center;
    justify-Content: center;
    border-radius: 10px;
    border-Color: #B7460C;
    margin-top: 10px;
    margin-left: 10px;
    background-color: #FFB500;
`;

