import React, {useState} from 'react';
import { Text, View } from 'react-native';
import { Ionicons } from "@expo/vector-icons";
import { Modalize } from 'react-native-modalize';
import RecordCategoryModal from '../../components/Modal/ModalRecordCategory';
import RecordSubCategoryModal from '../../components/Modal/ModalRecordSubCategory';
import RecordTagModal from '../../components/Modal/ModalRecordTag';
import RecordAccountModal from '../../components/Modal/ModalRecordAccount';
import RecordMovementModal from '../../components/Modal/ModalRecordMovement';
import DetailsTransactionModal from '../../components/Modal/ModalDetailsTransaction';
import { Container, Button } from './styles';

export default () => {
  const [modal, setModal] = useState('');
  const [toggle, setToggle] = useState(true);
  
  const modalizeRef = React.createRef();

  const onOpen = (modalOpen) => {
    setModal(modalOpen)
    modalizeRef.current.open();
  };

  return (
    <Container>
      <View style={{ flexDirection: 'row' }}>
        <Button
          onPress={() => onOpen('category')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="albums"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 16 }}
            >
              Categoria
            </Text>
          </View>
        </Button>

        <Button
          onPress={() => onOpen('subcategory')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="albums-outline"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 16 }}
            >
              Sub-Categoria
            </Text>
          </View>
        </Button>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Button
          onPress={() => onOpen('tag')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="md-pricetags-sharp"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 16 }}
            >
              TAG
            </Text>
          </View>
        </Button>
        <Button
          onPress={() => onOpen('account')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="card"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 16 }}
            >
              Conta
            </Text>
          </View>
        </Button>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Button
          onPress={() => console.log('In construction')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="cash-outline"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 16 }}
            >
              Orçamentos
            </Text>
          </View>
        </Button>
        <Button style={{ color: 'red' }}
          onPress={() => onOpen('movement')}
        >
          <View
            style={{
              alignItems: 'center'
              , justifyContent: 'center'
            }}
          >
            <Ionicons
              name="add-circle"
              size={50} color={'#B7460C'}
            />

            <Text
              style={{ fontSize: 18 }}
            >
              Movimentos
            </Text>
          </View>
        </Button>
      </View>

      <Modalize ref={modalizeRef} adjustToContentHeight={toggle}>
        {modal == 'category' && (<RecordCategoryModal modalize={modalizeRef} />)}
        {modal == 'subcategory' && (<RecordSubCategoryModal modalize={modalizeRef} />)}
        {modal == 'tag' && (<RecordTagModal modalize={modalizeRef} />)}
        {modal == 'account' && (<RecordAccountModal modalize={modalizeRef} />)}
        {modal == 'movement' && (<RecordMovementModal modalize={modalizeRef} />)}
        {modal == null && (null)}
      </Modalize>
    </Container>
  );
};