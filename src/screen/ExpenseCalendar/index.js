import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {loadingMovement, getMovement, selectMovement} from '../../reducers/movementSlice';
import { Agenda } from 'react-native-calendars';
import Firebase from '../../firebase/firebase';
// import PropTypes from 'prop-types';
import { groupBy } from 'lodash';
import ExpenseItem from '../../components/ExpenseItem';
import styles from './styles';
import { LocaleConfig } from './language';

// const renderKnob = () => {
//   return (
//     <View
//       style={{
//         flexDirection: 'row'
//         , alignItems: 'center'
//       }}>
//       <Text style={styles.agenda.knobText}>Mais datas</Text>
//       <Entypo name="chevron-small-down"
//         style={styles.agenda.knobIcon}
//       />
//     </View>
//   );
// };

// const getDerivedStateFromProps = (props) => {
//   if (!props.eventsLoading && !props.eventsError) {
//     const eventsWithColor = props.events.map((obj, index) => {
//       return {
//         ...obj,
//         color: index
//       };
//     });
//     const eventsGroupedByDate = groupBy(eventsWithColor, 'date');
//     return {
//       events: eventsGroupedByDate,
//     };
//   }
//   return null;
// }



const ExpensesCalendar= (props) => {

  const [month, setMonth] = useState(null);
  let objects = useSelector(selectMovement);
  
  const dispatch = useDispatch();

  useEffect(() => {
    const getMovements = () => {
      Firebase.GetMovement(month).then(obj => {
        dispatch(getMovement(groupBy(obj, 'date')));    
      })
    }
    getMovements();
  }, [month, objects.eventsDeleted])
 
  const renderItem = (item) => {
    return( 
            <ExpenseItem movement={item} 
                         style={styles.agenda.item} 
           />
          )
  };

  const renderEmptyData = (props) => {
    return (
      <View style={styles.emptyContainer}>
         {
         objects.eventsLoading 
         ? (<View color={'#FF3366'} />) 
         : (<Text style={styles.emptyMsg}>Não há eventos para está data.</Text>)
         }
      </View>
    );
  };

  return (
    
    <View style={{ flex: 1, marginTop: 20 }}>
      <Agenda
        items={objects.movement}
        showOnlySelectedDayItems={true}
        loadItemsForMonth={date => { setMonth(date.dateString) }}
        pastScrollRange={3}
        futureScrollRange={3}
        renderItem={renderItem}
        renderEmptyDate={() => { return (<View ><View style={styles.agenda.itemEmpty} /></View>); }}
        renderEmptyData={() => renderEmptyData()}
        rowHasChanged={(r1, r2) => { return r1.text !== r2.text }}
        hideKnob={false}
        showClosingKnob={true}
        animateScroll={true}
        enableSwipeMonths={true}
        onRefresh={() => console.log('refreshing...')}
        //onRefresh={date => { props.getEvents(date) }}
        theme={{
          calendarBackground: '#FFA500',
          textSectionTitleColor: '#D2691E',
          selectedDayBackgroundColor: '#D2691E',
          selectedDayTextColor: '#FAFAFA',
          todayTextColor: '#FF4500',
          textDisabledColor: '#DDD',
          dotColor: '#F0E68C',
          selectedDotColor: '#FAFAFA',
          arrowColor: '#FF3366',
          monthTextColor: '#8A4B08',
          dayTextColor: '#8A4B08',
          agendaDayNumColor: '#FFA500',
          agendaDayTextColor: '#FFA500',
          agendaTodayColor: '#DF3A01',
        }}
        // Agenda container style
        style={styles.agenda.container}
      />
    </View>
  );
}

export default ExpensesCalendar;

//Define e tipifica as propriedades deste componente
// ExpensesCalendar.propTypes = {
//   navigation: PropTypes.any,
//   selected: PropTypes.string,
//   getEvents: PropTypes.func.isRequired,
//   eventsLoading: PropTypes.bool.isRequired,
//   eventsDeleted: PropTypes.bool.isRequired,
//   eventsError: PropTypes.bool.isRequired,
//   events: PropTypes.object,
// };

//Define um valor default para as propriedades
// ExpensesCalendar.defaultProps = {
//   eventsLoading: false,
//   eventsError: false,
//   eventsDeleted:false
// };


