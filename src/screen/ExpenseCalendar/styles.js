import { Dimensions } from 'react-native';
const deviceHeight = Dimensions.get('window').height;

export default {
  container: {
    flex: 1,
    width: null,
    height: null,
  },
  content: {
    backgroundColor: 'transparent',
  },
  emptyContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyMsg: {
    color: '#FFBF00',
    fontSize: 18,
    alignSelf: 'center',
    paddingBottom: 10,
  },
  agenda: {
    container: {
     
    },
    item: {
      flex: 1,
      backgroundColor: '#FFBF00',
      borderRadius: 3,
      marginRight: 10,
      marginLeft: 0,
      marginTop: 20,
    },
    itemEmpty: {
      backgroundColor: '#FFBF00',
      borderRadius: 3,
      marginRight: 25,
      marginLeft: 15,
      marginTop: 50,
      height: 1,
      alignItems: 'center'
    },
    knobText: {
      color: '#151515',
      fontSize: 13,
    },
    knobIcon: {
      color: '#151515',
      paddingLeft: 5,
      fontSize: 20,
    },
  },
};
