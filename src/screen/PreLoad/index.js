import React, { useLayoutEffect } from 'react';
import { Text, Image } from 'react-native';
import Firebase from '../../firebase/firebase';
import { useNavigation } from '@react-navigation/native';
import { Container, LoadingIcon } from './styles';
import BabyYoda from '../../assets/baby-yoda.png';


export default () => {

    const navigation = useNavigation();

    useLayoutEffect(() => {
        const checkToken = async () => {
            Firebase.checkToken(navigation);
        }
        checkToken();
    }, [])

    return (
        <Container>
            <Image
                source={BabyYoda}
                resizeMode="contain"
                style={{ width: "60%", height: "60%" }} />
            <LoadingIcon size="large" color="#FFFFFF" />
        </Container>
    )
}