import React, { useState } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { Container, CreditCardWrapper, CreditCardNumber, CreditCardExpiryDate, CreditCardBackground } from './styles';
import { StatusBar } from 'react-native';

function CreditCard({ cardNumber, expiryDate }) {
  return (
    <CreditCardWrapper style={{ backgroundColor: '#FFF', padding: 16 }}>
      <Text>{cardNumber}</Text>
      <Text>{expiryDate}</Text>
    </CreditCardWrapper>
  );
}

export default function CreditCardCarousel() {
  const [creditCards, setCreditCards] = useState([
    { id: 1, cardNumber: '**** **** **** 1234', expiryDate: '01/23' },
    { id: 2, cardNumber: '**** **** **** 5678', expiryDate: '03/25' },
    { id: 3, cardNumber: '**** **** **** 9012', expiryDate: '05/27' },
  ]);

  return (
    <Container>
      <Carousel style={CreditCardWrapper}
                data={creditCards}
                renderItem={({ item }) =>           
                    <CreditCardWrapper>
                        <CreditCardNumber>
                          {item.cardNumber}
                        </CreditCardNumber>
                        <CreditCardExpiryDate>
                          {item.expiryDate}
                        </CreditCardExpiryDate>
                    </CreditCardWrapper>}
        sliderWidth={350}
        itemWidth={350}
      />

    </Container>
  );
}


