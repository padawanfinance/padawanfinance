import React from 'react';
import styled from 'styled-components/native';
import { StatusBar } from 'react-native';

export const Container = styled.SafeAreaView`
    background-color: #FFA500;
    flex: 1;
    justify-content: center;
    align-items: center;
    padding-top: ${StatusBar.currentHeight}px;
`;

export const CreditCardWrapper = styled.View`
  width: 350px;
  height: 200px;
  background-color: #fff;
  margin-top: 10px;
  border-radius: 10px;
  padding: 16px;
  position: relative;
  box-shadow: 0px 0px 1px;
  elevation: 10;
}
`;

export const CreditCardNumber = styled.Text`
  font-size: 20px;
  margin-bottom: 8px;
  
`;

export const CreditCardExpiryDate = styled.Text`
  font-size: 16px;
  position: absolute;
  bottom: 16px;
  right: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

