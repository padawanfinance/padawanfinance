import React, { useState } from 'react';
import { useNavigation } from '@react-navigation/native';
import Firebase from '../../firebase/firebase';
import BabyYoda from '../../assets/baby-yoda.png';
import SignInput from '../../components/SignInput';
import emailIcon from '../../assets/email';
import lockIcon from '../../assets/lock';
import { validateEmail } from '../../functions/emailValidate';

import {
    Container
    , Image
    , InputArea
    , CustomButton
    , CustomButtonText
    , SignMessageButton
    , SignMessageButtonText
    , SignMessageButtonTextBold
} from './styles';

export default () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const navigation = useNavigation();

    const handleSignClick = () => {
        if (validateEmail(email)) {
            Firebase.SignIn(email, password, navigation);
        } else {
            alert("e-mail inválido!")
        };
    }

    const handleMessageButtonClick = () => {
        navigation.reset({
            routes: [{ name: "SignUp" }]
        })
    }

    return (
        <Container>
            <Image
                source={BabyYoda}
                resizeMode="contain"
            />

            <InputArea>
                <SignInput
                    IconSvg={emailIcon}
                    placeholder="Digite seu e-mail"
                    value={email}
                    onChangeText={txt => setEmail(txt)}
                />
                <SignInput
                    IconSvg={lockIcon}
                    placeholder="Digite sea senha"
                    value={password}
                    onChangeText={txt => setPassword(txt)}
                    password={true}
                />

                <CustomButton onPress={handleSignClick}>
                    <CustomButtonText>LOGIN</CustomButtonText>
                </CustomButton>
            </InputArea>

            <SignMessageButton onPress={handleMessageButtonClick}>
                <SignMessageButtonText>Ainda não possui uma conta?</SignMessageButtonText>
                <SignMessageButtonTextBold>Cadastre-se.</SignMessageButtonTextBold>
            </SignMessageButton>
        </Container>
    )
}