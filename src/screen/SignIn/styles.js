import React from 'react';
import styled from 'styled-components/native';

export const Image = styled.Image`
    width: 60%;
    height: 40%;
    margin-top: 100px;
    margin-bottom: 10px;
`;

export const Container = styled.SafeAreaView`
    background-color: #FFA500;
    flex: 1;
    justify-content: center;
    align-items: center;
`;

export const InputArea = styled.View`
    padding-left: 40px;
    padding-right: 40px;
`;

export const CustomButton = styled.TouchableOpacity`
    height: 60px;
    background-color: #D96704;
    border-radius: 30px;
    justify-content: center;
    align-items: center;
`;

export const CustomButtonText = styled.Text`
    font-size: 18px;
    color: #F29F05;
    font-weight: bold;
`;

export const SignMessageButton = styled.TouchableOpacity`
    flex-direction: row;
    justify-content: center;
    margin-top: 20px;
    margin-bottom: 100px;
`;

export const SignMessageButtonText = styled.Text`
    font-size: 13px;
    color: #771513;
`;

export const SignMessageButtonTextBold = styled.Text`
    font-size: 13px;
    color: #771513;
    font-weight: bold;
    margin-left: 5px;
`;
